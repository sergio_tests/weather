# weather

## Описание

API сервис, который выводит данные о ежедневной температуре в Москве на время 14:00 на столько ближайших дней, сколько возможно, используя API метеорологической службы yr.no.

Сервер реализован на базе python-фреймворка [fastapi](https://fastapi.tiangolo.com/).

Сервис содержит один эндпоинт:

~~~
[GET] /api/v1/weather/forecast/
~~~

### Пример запроса

~~~
curl -X GET "http://127.0.0.1:8000/api/v1/weather/forecast/" -H  "accept: application/json"
~~~


## Запуск в docker

_При первом использовании нужно настроить .env файл_

~~~
# Копируем шаблоны .env файлов 
cp .dev.env .env

# ... и редактируем нужные настройки
~~~

**Сборка и запуск контейнера**

~~~shell
# Сборка
docker-compose build

# Блокирующий запуск в котором видны логи
docker-compose up

# ... или фоновый запуск
docker-compose up -d 
~~~

## Установка без docker

1.  Клонировать проект
2.  Скопировать файлы окружения `.dev.env -> .env`
3.  Установить зависимости на сервере `pip3 install -r requirements.txt`
4.  Запуск сервера: `uvicorn weather.server:app`


## Конфигурация

Пример настройки в файле `.env`:
```
YR_API_URL="https://api.met.no/weatherapi/"
YR_API_RESOURCE="locationforecast/2.0/compact"
YR_API_HEADER_UA="MyUniqueWeatherApp/1.0.1 (https://myweatherapp.example.com; admin@myweatherapp.example.com)"

FORECAST_LAT=-34.5677265
FORECAST_LON=-58.4515826
FORECAST_LOCAL_TZ="Europe/Moscow"
FORECAST_LOCAL_TIME="14:00"
```
