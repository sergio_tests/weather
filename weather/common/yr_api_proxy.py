import logging

import uplink
from uplink import error_handler, Consumer, get, QueryMap, Path
from uplink.retry.when import raises, status, status_5xx
from uplink_httpx import HttpxClient

from weather import config
from weather.http_error import HttpException

logger = logging.getLogger(__name__)


def raise_api_error(exc_type, exc_val, exc_tb):
    logger.error(exc_val)
    raise HttpException(status=500, message=exc_val)


def raise_for_status(consumer, response):
    msg = f'url:{response.url} code:{response.status_code}'
    if 200 <= response.status_code < 300:
        if config.DEBUG:
            logger.debug(f'Response: {msg} text:{response.text[:500]}')
        return response

    logger.error(f'Response: {msg} text:{response.text}')
    raise HttpException(status=response.status_code, message=response.text[:500])


@uplink.headers({"User-Agent": config.YR_API_HEADER_UA})
@uplink.timeout(90)
@uplink.retry(when=raises(Exception) | status(403) | status_5xx(), max_attempts=5)
@uplink.returns.json()
@error_handler(raise_api_error)
@uplink.response_handler(raise_for_status, requires_consumer=True)
class YrBaseAPIProxy(Consumer):

    def __init__(self):
        super().__init__(base_url=config.YR_API_URL, client=HttpxClient)

    @get("{resource}")
    async def get_resource(self, resource: uplink.Path, params: QueryMap):
        pass


class YrAPIProxy(YrBaseAPIProxy):
    async def get_forecast_by_coordinates(self, latitude: float, longitude: float):
        params = {"lat": latitude, "lon": longitude}
        json_response = await self.get_resource(resource=config.YR_API_RESOURCE, params=params)
        return json_response



