from typing import List

from pydantic import BaseModel


class WeatherUnits(BaseModel):
    air_temperature: str | float


class WeatherForecastData(BaseModel):
    local_time: str
    values: WeatherUnits


class WeatherForecast(BaseModel):
    units: WeatherUnits
    forecasts_by_local_time: List[WeatherForecastData]


__all__ = [
    "WeatherUnits",
    "WeatherForecastData",
    "WeatherForecast",
]
