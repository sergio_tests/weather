from datetime import datetime
from typing import List

from pydantic import BaseModel


class YrUnits(BaseModel):
    air_pressure_at_sea_level: str
    air_temperature: str
    cloud_area_fraction: str
    precipitation_amount: str
    relative_humidity: str
    wind_from_direction: str
    wind_speed: str
    
    
class YrValues(BaseModel):
    air_pressure_at_sea_level: float = None
    air_temperature: float = None
    cloud_area_fraction: float = None
    relative_humidity: float = None
    wind_from_direction: float = None
    wind_speed: float = None


class YrTimeSeriesDataInstantItem(BaseModel):
    details: YrValues


class YrTimeSeriesData(BaseModel):
    instant: YrTimeSeriesDataInstantItem
    next_1_hours: dict | None = None
    next_6_hours: dict | None = None
    next_12_hours: dict | None = None


class YrTimeSeriesItem(BaseModel):
    time: datetime
    data: YrTimeSeriesData


class YrMeta(BaseModel):
    updated_at: str
    units: YrUnits


class YrProperties(BaseModel):
    meta: YrMeta
    timeseries: List[YrTimeSeriesItem]

