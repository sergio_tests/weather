from fastapi import FastAPI
from fastapi.exceptions import RequestValidationError
from starlette.middleware.cors import CORSMiddleware

from weather.api.api_router import api_router
from weather.http_error import return_http_error, process_validation_error, HttpException

app = FastAPI(
    title='Weather API Server',
    version='1.0.0',
    docs_url='/api/v1/docs',
    redoc_url='/api/v1/redoc',
    openapi_url='/api/v1/openapi.json'
)

origins = ["*"]
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.exception_handler(Exception)(return_http_error)
app.exception_handler(HttpException)(return_http_error)
app.exception_handler(RequestValidationError)(process_validation_error)

app.include_router(api_router, prefix='/api/v1')


def run_server():
    from weather import config
    import uvicorn
    uvicorn.run(app, host="0.0.0.0", port=8000, reload=config.DEBUG)


if __name__ == "__main__":
    run_server()
