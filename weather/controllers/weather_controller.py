import bisect
from datetime import timedelta, datetime
from typing import List

import pandas as pd
import pytz
from pydash import py_

from weather import config
from weather.common.yr_api_proxy import YrAPIProxy
from weather.shemas.weather_forecast import WeatherForecast, WeatherUnits, WeatherForecastData
from weather.shemas.yr_service import YrProperties, YrValues


class WeatherController:
    def __init__(self):
        self.yr_api = YrAPIProxy()

    async def get_forecast(self) -> WeatherForecast:
        json_response = await self.yr_api.get_forecast_by_coordinates(latitude=config.FORECAST_LAT,
                                                                      longitude=config.FORECAST_LON)

        yr_props: YrProperties = YrProperties.parse_obj(py_.get(json_response, "properties", {}))

        forecast_time = pd.to_datetime(config.FORECAST_LOCAL_TIME).tz_localize(pytz.timezone(config.FORECAST_LOCAL_TZ))

        forecasts_data: List[WeatherForecastData] = []
        for dt, forecast_data in py_.group_by(yr_props.timeseries, lambda i: i.time.date()).items():
            if len(forecast_data) < 2:
                continue

            target_datetime = datetime.combine(dt, forecast_time.astimezone(pytz.utc).time(), tzinfo=pytz.utc)
            forecast_dt_list = [d.time for d in forecast_data]
            index = bisect.bisect_left(forecast_dt_list, target_datetime)  # находим ближайшее значение
            t_before = forecast_dt_list[index - 1] if index > 0 else None
            t_after = forecast_dt_list[index] if index < len(forecast_dt_list) else None
            if not t_before or not t_after:
                continue

            temp_before = forecast_data[index - 1].data.instant.details.air_temperature
            temp_after = forecast_data[index].data.instant.details.air_temperature
            interp_temp = temp_before + (temp_after - temp_before) * (
                        (forecast_time.hour - t_before.hour) / (t_after.hour - t_before.hour))

            forecasts_data.append(WeatherForecastData(
                local_time=forecast_time.time().isoformat(),
                values=WeatherUnits(air_temperature=interp_temp),
            ))

        response = WeatherForecast(
            units=WeatherUnits.parse_obj(yr_props.meta.units.dict()),
            forecasts_by_local_time=forecasts_data,
        )
        return response

