import logging
import os
from pathlib import Path

from starlette.config import Config
from starlette.datastructures import URL, URLPath


# Load environment variables from .env file
APP_ROOT = Path(__file__).resolve().absolute().parent.parent
CONFIG_ROOT = Path(os.environ.get('WEATHER_CONFIG_ROOT') or APP_ROOT)
ENV_FILE = CONFIG_ROOT / ".env"
CONFIG = Config(ENV_FILE)

DEBUG = CONFIG.get('WEATHER_DEBUG', bool, default=False)

YR_API_URL = CONFIG.get('YR_API_URL', cast=URL)
YR_API_RESOURCE = CONFIG.get('YR_API_RESOURCE', cast=URLPath)
YR_API_HEADER_UA = CONFIG.get('YR_API_HEADER_UA', str, default="Mozilla/5.0")

FORECAST_LAT = CONFIG.get('FORECAST_LAT', float)
FORECAST_LON = CONFIG.get('FORECAST_LON', float)
FORECAST_LOCAL_TZ = CONFIG.get('FORECAST_LOCAL_TZ', str)
FORECAST_LOCAL_TIME = CONFIG.get('FORECAST_LOCAL_TIME', str)

logging.basicConfig(
    level=logging.ERROR,
    format='[%(asctime)s %(levelname)s] %(message)s',
    datefmt='%Y-%m-%d %H:%M'
)
