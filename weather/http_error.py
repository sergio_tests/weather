from starlette.requests import Request
from starlette.responses import JSONResponse
from fastapi.exceptions import RequestValidationError


class HttpException(Exception):
    def __init__(self, status: int, message: str):
        self.status = status
        self.message = message

    def __str__(self):
        return f'HttpException({self.status}, "{self.message}")'


def return_http_error(request: Request, exc: HttpException) -> JSONResponse:
    error_message = exc.message
    response = JSONResponse({'type': 'error', 'message': error_message, 'status': exc.status}, status_code=exc.status)
    request.state.error_message = error_message
    return response


def process_validation_error(request: Request, exc: RequestValidationError):
    error_message = str(exc)
    response = JSONResponse({'type': 'error', 'message': error_message, 'status': 422}, status_code=422)
    request.state.error_message = error_message
    return response

