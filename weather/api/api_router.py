from fastapi import APIRouter, Depends

from weather.controllers.weather_controller import WeatherController
from weather.shemas.weather_forecast import WeatherForecast


api_router = APIRouter()


@api_router.get("/weather/forecast/", response_model=WeatherForecast)
async def get_weather_forecast(controller: WeatherController = Depends(WeatherController)):
    forecast = await controller.get_forecast()
    return forecast


__all__ = [
    'api_router',
]
