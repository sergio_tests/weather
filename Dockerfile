FROM python:3.12

ENV LANG=ru_RU.UTF-8 \
    LC_CTYPE=ru_RU.UTF-8 \
    LANGUAGE=ru_RU.UTF-8 \
    LC_ALL=ru_RU.UTF-8 \
    TZ=UTC \
    DEBIAN_FRONTEND=noninteractive \
    PYTHONPATH="${PYTHONPATH}:/app"

WORKDIR /app

RUN apt-get update \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

COPY requirements.txt /app/requirements.txt
RUN pip3 install --no-cache-dir -r requirements.txt

COPY . /app/

ENTRYPOINT ["uvicorn"]
CMD ["weather.server:app"]
